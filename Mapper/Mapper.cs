using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Mapper
{

    public class Mapper<TSource, TDestination>
    {
        Func<TSource, TDestination> mapFunction;

        internal Mapper(Func<TSource, TDestination> func)
        {
            mapFunction = func;
        }

        public TDestination Map(TSource source)
        {
            return mapFunction(source);
        }
    }

    public class MappingGenerator
    {
        public Mapper<TSource, TDestination> Generate<TSource, TDestination>()
        {
            var countOfFields = Expression.Variable(typeof(int), "count");
            var sourceParam = Expression.Parameter(typeof(TSource), "source");
            var destinationObj = Expression.Variable(typeof(TDestination), "d");
            var label = Expression.Label(typeof(TDestination));
            var iArgument = Expression.Variable(typeof(int), "i");
          
            var getCountMethod = typeof(Help).
                GetMethod("GetPropertyInfoCount", new[] {typeof(PropertyInfo[])});
            var method = typeof(Type).GetMethod("GetProperties", new Type[0]);

            var initialBlock = Expression.Block(
                Expression.Assign(destinationObj, Expression.New(typeof(TDestination))),
                Expression.Assign(iArgument, Expression.Constant(0)),
                Expression.Assign(countOfFields, Expression.Call(
                    getCountMethod, 
                    Expression.Call(Expression.Constant(typeof(TDestination)), method))
                )
            );
            
            var mapExpression = Expression.Block(
                new[] {iArgument, destinationObj, countOfFields},
                initialBlock,
                Expression.Loop(
                    Expression.IfThenElse(
                        Expression.LessThan(iArgument, countOfFields),
                        GetLoopExpression<TSource, TDestination>(iArgument, destinationObj, sourceParam),
                        Expression.Break(label, destinationObj)
                    ),
                    label
                 )
            );
            var mapFunction =
                Expression.Lambda<Func<TSource, TDestination>>(mapExpression, sourceParam);
            return new Mapper<TSource, TDestination>(mapFunction.Compile());
        }

        private BlockExpression GetLoopExpression<TSource, TDestination>(
            ParameterExpression iArgument, 
            ParameterExpression destinationObj, 
            ParameterExpression sourceParam
        )
        {
            var propertyInfo = Expression.Variable(typeof(PropertyInfo), "property");
            var sourceField = Expression.Variable(typeof(object), "sourceField");
            
            var getPropertyInfoMethod = typeof(Help).
                GetMethod("GetPropertyInfo", new[] {typeof(PropertyInfo[]), typeof(int)});
            var getValueMethod = typeof(Help).
                GetMethod("GetValue", new[] {typeof(PropertyInfo), typeof(object)});
            var setValueMethod = typeof(PropertyInfo).
                GetMethod("SetValue", new[] {typeof(object), typeof(object)});
            var method = typeof(Type).GetMethod("GetProperties", new Type[0]);

            var initialBlock = Expression.Block(
                Expression.Assign(propertyInfo,
                    Expression.Call(
                        getPropertyInfoMethod,
                        Expression.Call(Expression.Constant(typeof(TDestination)), method),
                        iArgument)
                ),
                Expression.Assign(
                    sourceField,
                    Expression.Call(getValueMethod, propertyInfo, sourceParam)
                )
            );
            
            return Expression.Block(
                new[] {propertyInfo, sourceField},
                initialBlock,
                Expression.Call(propertyInfo, setValueMethod, destinationObj, sourceField),
                Expression.PostIncrementAssign(iArgument)
            );
        }
    }

    public class Help
    {
        public static PropertyInfo GetPropertyInfo(PropertyInfo[] propertyInfoArray, int i)
        {
            var field = propertyInfoArray[i];
            return field;

        }
        public static int GetPropertyInfoCount(PropertyInfo[] propertyInfoArray)
        {
            var count = propertyInfoArray.Length;
            return count;
            //return 2;

        }

        public static object GetValue(PropertyInfo propertyInfo, object source)
        {
            return source.GetType().GetProperty(propertyInfo.Name).GetValue(source);
        }
    }
    public class Foo
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public override string ToString()
        {
            return String.Format($"Name: {Name}, Age: {Age}");
        }
    }

    public class Bar
    {
        public string Name { get; set; }
        public int Age { get; set; }
        
        public override string ToString()
        {
            return String.Format($"Name: {Name}, Age: {Age}");
        }
    }
}
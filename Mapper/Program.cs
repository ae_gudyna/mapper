﻿using System;

namespace Mapper
{
    class Program
    {
        static void Main(string[] args)
        {
            var mapGenerator = new MappingGenerator(); var mapper = mapGenerator.Generate<Foo, Bar>();
            
            var foo = new Foo();
            foo.Name = "Daisy";
            foo.Age = 12;
            
            var bar = mapper.Map(foo);
            Console.WriteLine($"foo: {foo}");
            Console.WriteLine($"bar: {bar}");
            Console.ReadLine();
        }
    }
}